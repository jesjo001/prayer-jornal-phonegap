//admob
function onDocLoad() {
        if(( /(ipad|iphone|ipod|android|windows phone)/i.test(navigator.userAgent) )) {
            document.addEventListener('init', initApp, false);
        } else {
            initApp();
        }
    }
    
    function initApp() {
        initAd();
        // display the banner at startup
        window.plugins.AdMob.createBannerView();
      
        // display the interstitial at startup
        window.plugins.AdMob.createInterstitialView();
    }

    function initAd(){
        if ( window.plugins && window.plugins.AdMob ) {
          var ad_units = {
        
        ios : {
          banner: 'ca-app-pub-6552658763120509/5882853786',
          interstitial: 'ca-app-pub-6552658763120509/1878431107'
        },
        android : {
          banner: 'ca-app-pub-6552658763120509/5882853786',
          interstitial: 'ca-app-pub-6552658763120509/1878431107'
        },
        wp8 : {
          banner: 'ca-app-pub-6552658763120509/5882853786',
          interstitial: 'ca-app-pub-6552658763120509/1878431107'
        }
      };

          var admobid = "";
          if( /(android)/i.test(navigator.userAgent)) {
            admobid = ad_units.android;
          } else if(/(iphone|ipad)/i.test(navigator.userAgent)) {
            admobid = ad_units.ios;
          } else {
            admobid = ad_units.wp8;
          }
          
            window.plugins.AdMob.setOptions({                
                publisherId: admobid.banner,
                interstitialAdId: admobid.interstitial,
                bannerAtTop: false, // set to true, to put banner at top
                overlap: true, // set to true, to allow banner overlap webview
                offsetTopBar: false, // set to true to avoid ios7 status bar overlap
                isTesting: true, // receiving test ad
                autoShow: true // auto show interstitial ad when loaded
            });
            registerAdEvents();
            
        } else{
            alert('admob plugin not ready');
        }
    }

    function onResize() {
        var msg = 'web view: ' + window.innerWidth + ' x ' + window.innerHeight;
        document.getElementById('sizeinfo').innerHTML = msg;
    }

var defaultcategories = new Array( "National", "Intersession", "Thanksgiving", "Personal");

var  optionExist = 0;
var currentPage = 'all'; 

if(typeof(Storage) !== "undefined"){
  if(!localStorage.catNumber){
      localStorage.setItem("catNumber", "0");
      optionExist = Number(localStorage.catNumber);
    }else{
      optionExist = Number(localStorage.catNumber);
    }

}else{
  console.log('Sorry Web storage not surported');
};

// use to disable menu open


window.fn = {};

window.fn.open = function () {
 //var content = document.querySelector('ons-splitter-content');
  var menu = document.getElementById('menu');

    //var menu = document.getElementById('menu');
    menu.open();
    

};
function openMenu(){
   var content = document.querySelector('ons-splitter-content');
  
    var menu = document.getElementById('menu');
    menu.open();
    
}

function close(){
    var menu = document.getElementById('menu');
    menu.close();
    
}

window.fn.load = function (page) {
  // var content = document.getElementById('content');
  // var menu = document.getElementById('menu');
  //  content.load(page)
  //  .then(menu.close.bind(menu));
   if(page == 'add.html'){

  var myNavigator = document.getElementById('myNavigator');
  myNavigator.pushPage('add.html');
}else{

var content =document.getElementById('content');
var menu = document.getElementById('menu');
content.load(page).then(menu.close.bind(menu));
}

};

function loadHome(){
  /*document.getElementById("tab1").active = "true";
  document.getElementById("tab2").active = "false";*/
  //$('ons-tab').getElementById('tab1').attr('active', 'true');
  var element = document.getElementById("tabA");
  element.active="true";
  //element.next();
}
 
window.fn.back = function(){
  //var myNavigator = document.getElementById('myNavigator');
  //myNavigator.pushPage('tabbar.html');
  var myNavigator = document.getElementById('myNavigator');
  myNavigator.pushPage('splitter.html');
 // fn.loadPage('all');
 // fn.loadPage('all');
 // fn.load('tabbar.html');
//menuOpen = true;

}

window.fn.loadPage = function(page){

//load content into page for all category
  if(page == 'all') {
    openCategoryDb();
    openDb();
    getItems();
    close();
    currentPage = page;
    refreshAnswered('all');
  }

//load content into page for unsorted category
  if(page == 'unsorted') {
    openCategoryDb();
    openDb();
    getCategorySort('unsorted');
    close();
    currentPage = page;
  }

//load content into page for National Prayer category
  if(page == 'National') {
    //fn.load('tabbar.html');
    
    openCategoryDb();
    openDb();
    getCategorySort('National');
    close();
    currentPage = page;
    refreshAnswered('National');
  }

  //event.target.id == "answered"
if(page == 'National' && event.target.id == "answered") {
    openCategoryDb();
    openDb();
    getAnswered('National');
    close();
    currentPage = page;
    refreshAnswered('National');
  }

//load content into page for Intersession Prayer category
  if(page == 'Intersession') {
    openCategoryDb();
    openDb();
    getCategorySort('Intersession');
    close();
    currentPage = page;
    refreshAnswered('Intersession');
  }

//load content into page for Thanksgiving Prayer category
  if(page == 'Thanksgiving') {
    openCategoryDb();
    openDb();
    getCategorySort('Thanksgiving');
    close();
    currentPage = page;
    refreshAnswered('Thanksgiving');
  }

//load content into page for Personal Prayer category
  if(page == 'Personal') {
    openCategoryDb();
    openDb();
    getCategorySort('Personal');
    close();
    currentPage = page;
    refreshAnswered('Personal');
  }

}

function refreshAnswered(page){

    openDb();
    getAnswered(page);
}


document.addEventListener('init', function(event){


  if(event.target.id == "home"){
    openDb();
    getItems();
  }

if(event.target.id == "answered"){
  if(currentPage = 'all'){
    openDb();
    getAnswered('all');

  }else if(currentPage == 'National'){
    openDb();
    getAnswered('National');
  }else if(currentPage == 'Personal'){
    openDb();
    getAnswered('Personal');
  }else if(currentPage == 'Intersession'){
    openDb();
    getAnswered('Intersession');
  }else if(currentPage == 'Thanksgiving'){
    openDb();
    getAnswered('Thanksgiving');
  }

}

//add Page Codes
if(event.target.id == "addPage"){
  openCategoryDb();
  renderList();      
  }

});

document.addEventListener('show', function(event){

if(event.target.id == "answered"){
      openDb();
      getAnswered(currentPage);
}

});

function renderList(){
  adb.transaction(function(tx){
    tx.executeSql("SELECT * FROM CategoryOption", [], renderCategory, onError);
  });
}



var ydb = null;  //pRAYEER DB
var db2 = null; //aNSWERED PRAYER DB
var cdb = null; //
var adb = null; //CATEGORY DB

function onError(tx, e){
  alert("something went wrong:" + e.Message);  

}

function onSuccess(tx, r){
 getItems();
}

function openDb(){
  //PRAYER DATABASE CREATION
  ydb = openDatabase("PrayerListA", "1", "Shopping list", 1024*1024);

  ydb.transaction(function(tx){                                      //item, date, category, memoryverse 
    tx.executeSql("CREATE TABLE IF NOT EXISTS items (ID INTEGER PRIMARY KEY ASC, item TEXT, date DATETIME, category TEXT, memoryverse TEXT)", [],
      function(sqlT, sqlR){
        console.log("PrayerList db created, items table created");
      }, function(sqlT, e){
        console.log("error creating db " + e.Message );
      });
  });

  //ANSWERED PRAYERS DTABASE
  db2 = openDatabase("AnsweredPrayersA", "1", "Answered Prayers", 1024*1024);

  db2.transaction(function(tx){
    tx.executeSql("CREATE TABLE IF NOT EXISTS items (ID INTEGER PRIMARY KEY ASC, item TEXT, date DATETIME, category TEXT, memoryverse TEXT)", []);
  });
}



//create a category database to store the ctegories available
function openCategoryDb(){
  adb = openDatabase("CategoryList", "1", "Category list", 1024*1024);

  adb.transaction(function(tx){
    tx.executeSql("CREATE TABLE IF NOT EXISTS CategoryOption (ID INTEGER PRIMARY KEY ASC, defaultCat TEXT, userCat TEXT)", []);
    if(optionExist < 1){
          tx.executeSql('INSERT INTO CategoryOption ( defaultCat) VALUES ( "National")');
          tx.executeSql('INSERT INTO CategoryOption ( defaultCat) VALUES ( "Intersession")');
          tx.executeSql('INSERT INTO CategoryOption ( defaultCat) VALUES ( "Thanksgiving")');
          tx.executeSql('INSERT INTO CategoryOption ( defaultCat) VALUES ( "Personal")');
     localStorage.catNumber = "2";
    // optionExist += 1;
     }
  });


}

function catSuccess(tx, r){

}

function createCategory(cat){
//......... code to create gategory
 adb.transaction(function(tx){
     tx.executeSql("INSERT INTO CategoryOption (userCat) VALUES (?)", [row.item], onSuccess, onError);
  });


}


//sort data to dispLAY
//DISPLAY ALL PRAYERS 
function getItems(){
  ydb.transaction(function(tx){
    tx.executeSql("SELECT * FROM items", [], renderItems, onError);
  });

}

//sort data to disp by categories
//DISPLAYS PRAYER BY CATEGORIES
function getCategorySort(category){
var cat = category;

  ydb.transaction(function(tx){
    tx.executeSql("SELECT * FROM items WHERE category=?", [category], renderItems, onError);
  });

}



//RENDER DATA TO PAGE
//CALLED AFTER EACH DB TRANSACTION 
function renderItems(tx, rs){
  var output ="";
  var list = document.getElementById('ongoingPrayer');
  
  for(i = 0; i < rs.rows.length; i++){
    var row = rs.rows.item(i);
    output += "<ons-list-item tappable onclick='appendItems("+ row.ID+ ")'><div style=\"width:80%;\">" + row.item + "</div>" +
    "<p id=\"number"+row.ID +"\" > </p>" +
    "<div class=\"left\" style=\" list-style-type: none;\" > </div>" + 
    "</ons-list-item>";

  }

  list.innerHTML = output;
};

//Render items for add page 'category'
function renderCategory(tx, rs){
    var output ="";
    var optioList = document.getElementById('select');

 for ( i =0; i<rs.rows.length; i++) { 
      var row = rs.rows.item(i)
      output += "<option>" + row.defaultCat +"</option>"; 
    }

    optioList.innerHTML = output;

}

//Answered pages codes

function getAnswered(category){
  //if currentPage
  if(category == 'all'){

    db2.transaction(function(tx){
    tx.executeSql("SELECT * FROM items", [], renderItemsAnswered, onError);
  });

  }

  if(category !== 'all'){
    db2.transaction(function(tx){
    tx.executeSql("SELECT * FROM items WHERE category=?", [category], renderItemsAnswered, onError);
  });

  }

  

}

function renderItemsAnswered(tx, rs){
  var output ="";
  var list = document.getElementById('answeredPrayers');

  for(i = 0; i < rs.rows.length; i++){
    var row = rs.rows.item(i);
    output += "<ons-list-item tappable>" + row.item + 
    "<div class=\"right\" > <ons-button onclick='deleteAnsweredItem(" + row.ID + ");'><ons-icon icon=\"trash\"></ons-icon></ons-button></div>" + 
    "</ons-list-item>";
  }

  list.innerHTML = output;
};

function deleteAnsweredItem(id){

   db2.transaction(function(tx){
   tx.executeSql("DELETE FROM items WHERE ID=?", [id], onSuccessAnswered, onError);

 });

};

function onSuccessAnswered(tx, r){
 getAnswered();

};
// select answered prayer and call insetTo function
// to inset into anwered db  
function answered(id){

  ydb.transaction(function(tx){
      tx.executeSql("SELECT * FROM items WHERE ID=?", [id], insertToAnswered, onError);
  
  });

}

//function insert answered prayers int answered db
function insertToAnswered(tx, rs){
  
  for(i = 0; i < rs.rows.length; i++){
    var row = rs.rows.item(i);

   db2.transaction(function(tx){
     tx.executeSql("INSERT INTO items (item, date, category, memoryverse) VALUES (?,?,?,?)", [row.item, row.date, row.category, row.memoryverse], onSuccess, onError);
   });

   deleteItem(row.ID);
  }
  
};



function appendItems(id){
  var elmentID = id;
  var output ="";
  var list = document.getElementById('number'+id);
  

 if(list.style.display === 'none'){

  ydb.transaction(function(tx){
      tx.executeSql("SELECT * FROM items WHERE ID=?", [id], function(tx,rs){

        for(i = 0; i < rs.rows.length; i++){
          var row = rs.rows.item(i);
          output += "<div class=\"left\" style=\"width:80%; padding:none; margin-top:5px; margin-bottom:1px;\"> Categoty : " + row.category + "<br /> Date : " + row.date + "<br />  Memory Verse : " + row.mv +"</div>"+
          "<div class=\"right\" style=\" float:right; list-style-type:none; width:100px; position:absolute; top: 20px; left:85%; \">"+
    "<ons-button onclick='deleteItem(" + id + ");'><ons-icon icon=\"trash\"></ons-icon></ons-button> <br /><br />" +
    "<ons-button onclick='answered(" + id +");' id=\""+ id + "\"><ons-icon icon=\"ion-android-checkbox-outline\" size=\"20px\" fixes-width=\"false\"></ons-icon></ons-button>" + 
    "</div>";

list.innerHTML = output;

    list.style.display = 'block';
        }

      }, onError);
  
  });

 /*   output += 
    "<div class=\"center\"><br /> "+
    "<ons-button onclick='deleteItem(" + id + ");'><ons-icon icon=\"trash\"></ons-icon></ons-button> <br /><br />" +
    "<ons-button onclick='answered(" + id +");' id=\""+ id + "\"><ons-icon icon=\"ion-android-checkbox-outline\" size=\"20px\" fixes-width=\"false\"></ons-icon></ons-button></div>" + 
    "</div>";

*/

   /*
    output += "<div class=\"center\"><br /> "+
    "<ons-button onclick='deleteItem(" + id + ");'><ons-icon icon=\"trash\"></ons-icon></ons-button> <br /><br />" +
    "<ons-button onclick='answered(" + id +");' id=\""+ id + "\"><ons-icon icon=\"ion-android-checkbox-outline\" size=\"20px\" fixes-width=\"false\"></ons-icon></ons-button></div>" + 
    "</div>";
   */ 

    

 }else{

  output += "<div class=\"center\"><br /> "+
    "<ons-button onclick='deleteItem(" + id + ");'><ons-icon icon=\"trash\"></ons-icon></ons-button> <br /><br />" +
    "<ons-button onclick='answered(" + id +");' id=\""+ id + "\"><ons-icon icon=\"done\"></ons-icon></ons-button></div>" + 
    "</div>";

    list.innerHTML = output;
    
    list.style.display = 'none';

 }

   // var row = rs.rows.item(i);
    
  

  
}

function addItem(){

  var textbox = document.getElementById('item');
  var datePicker = document.getElementById('Date');
  var mv = document.getElementById('mv');
  var cate = document.getElementById('select');

  var value = textbox.value;
  var valueDate = datePicker.value; //
  var valueMv = mv.value;
  var valueCate = cate.value;


/*
try{

  ydb.transaction(function(sqlTransactionSync){

    var sqlResultSet = sqlTransactionSync.executeSql(("INSERT INTO items (item, date, category, memoryverse ) VALUES (?,?,?,?);"),
     ["value", "12/05/2013", "National", "valueMv"]);
  })

console.log("Sucessfully inserted");

}catch (sqlException){
  postMessage("An error occured: " + sqlException.code + "(" + sqlException.message + ").");

}*/

  ydb.transaction(function(tx){
    tx.executeSql(("INSERT INTO items (item, date, category, memoryverse ) VALUES (?,?,?,?);"),
     [value, valueDate, valueCate, valueMv]);
  });
console.log(value + "date : "+ valueDate +" category:  " + valueCate +"  M  V" + valueMv );
  textbox.value = "";
   fn.load('tabbar.html');

  
};



function deleteItem(id){
 ydb.transaction(function(tx){
   tx.executeSql("DELETE FROM items WHERE ID=?", [id], onSuccess, onError);

 });
}

function deleteItemAnswered(id){
 db2.transaction(function(tx){
   tx.executeSql("DELETE FROM items WHERE ID=?", [id], onSuccess, onError);

 });
}


//Load Pages
function unsorted(){


}
